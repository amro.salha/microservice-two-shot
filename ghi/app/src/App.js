import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm'
import ShoesList from './ShoesList';
import HatList from './HatList';
// import EditShoeForm from './ShoeEdit';

function App(props) {
  // if (props.shoes === undefined){
  //   return null
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes/" element={<ShoesList shoes={props.shoes} />} />
          <Route path="shoe/new" element={<ShoeForm />} />
          <Route path="hats/" element={<HatList hats={props.hats} />} />
          <Route path="hats/new" element={<HatForm />}/>
          {/* <Route path="edit/:shoeId" element={<EditShoeForm />}/> */}
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
