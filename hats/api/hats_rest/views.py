from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Hat, LocationVO
import json

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ['id']

class HatsEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style", "color", "picture", "location"]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsEncoder,
        )

    if request.method == "POST":
        content = json.loads(request.body)
        try:
            id = content["location"]
            print(id)
            location = LocationVO.objects.get(id=id)
            content['location'] = location
            print(content)
            print(content['location'])
        except LocationVO.DoesNotExist:
            return JsonResponse({'message': 'Invalid location ID'}, status=400)
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsEncoder,
            safe=False,
        )


def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatsEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
