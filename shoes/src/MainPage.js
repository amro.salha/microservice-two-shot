import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function shoeColumns(props) {
  return (
    <div className="col">
      {props.list.map(data => {
        const conference = data.shoe;
        return (
          <div key={shoe.href} className="card mb-3 shadow">
            <img src={shoe.img_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{shoe.model_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {shoe.manufacturer}
              </h6>
              <p className="card-text">
                {shoe.color}
              </p>
            </div>
            <div className="card-footer">
            </div>
          </div>
        );
      })}
    </div>
  );
}

const MainPage = (props) =>  {
  const [shoeColumns, setShoeColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        // Get the list of conferences
        const data = await response.json();

        // Create a list of for all the requests and
        // add all of the requests to it
        const requests = [];
        for (let shoe of data.shoes) {
          const detailUrl = `http://localhost:8000${shoe.href}`;
          requests.push(fetch(detailUrl));
        }

        // Wait for all of the requests to finish
        // simultaneously
        const responses = await Promise.all(requests);

        // Set up the "columns" to put the conference
        // information into
        const columns = [[], [], []];

        // Loop over the conference detail responses and add
        // each to to the proper "column" if the response is
        // ok
        let i = 0;
        for (const shoeResponse of responses) {
          if (shoeResponse.ok) {
            const details = await shoeResponse.json();
            columns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(shoeResponse);
          }
        }

        // Set the state to the new list of three lists of
        // conferences
        setShoeColumns(columns);
      }
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
        <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
        <h1 className="display-5 fw-bold">Conference GO!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            The only resource you'll ever need to plan an run your in-person or
            virtual conference for thousands of attendees and presenters.
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/attendees/new" className="btn btn-primary btn-lg px-4 gap-3">Attend a conference</Link>
          </div>
        </div>
      </div>
      <div className="container">
        <h2>Upcoming conferences</h2>
        <div className="row">
          {conferenceColumns.map((conferenceList, index) => {
            return (
              <ConferenceColumn key={index} list={conferenceList} />
            );
          })}
        </div>
      </div>
    </>
  );
}

export default MainPage;
