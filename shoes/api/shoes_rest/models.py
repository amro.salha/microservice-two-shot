from django.db import models
from django.urls import reverse

# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    img_url = models.URLField(default='https://img.freepik.com/premium-vector/sneakers-outline-drawing-black-lines-sport-shoe-white-background_231873-1297.jpg')

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse('api_show_shoe', kwargs={"pk": self.pk})
