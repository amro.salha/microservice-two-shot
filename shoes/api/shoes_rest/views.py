from django.shortcuts import render

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ['id']

class ShoeListEncoder(ModelEncoder):
    model = Shoe,
    properties = [
        'manufacturer',
        'model_name',
    ]
    def get_extra_data(self, o):
        return { 'bin' : o.bin.id }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'img_url',
        'bin',
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        # if bin_vo_id is not None:
        #     shoes = Shoe.objects.filter(bin=bin_vo_id)
        # else:
        shoes = Shoe.objects.all()
        return JsonResponse(
            {'shoes' : shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            id = content["bin"]
            bin = BinVO.objects.get(id=id)
            content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({'message': 'Invalid bin ID'}, status=400)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
